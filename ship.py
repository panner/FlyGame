import pygame


class Ship():
    def __init__(self, ai_setting, screen):
        # 初始化飞船，并设置其初始化位置
        self.screen = screen
        # 移动标志
        self.moving_right = False
        self.moving_left = False

        # 加载飞船图片并获取其外接矩形
        self.image = pygame.image.load('images/ship.bmp')
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()

        self.ai_setting = ai_setting

        # 将每艘飞船放在屏幕的底部中央
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        # 在飞船的属性center中存储最小数值
        self.center = float(self.rect.centerx)

    def update(self):
        """根据移动标志调整飞船"""
        if self.moving_right and self.rect.right < self.screen_rect.right:
            # self.rect.centerx += 1
            # 更新飞船的center值，而不是rect
            self.center += self.ai_setting.ship_speed_factor
        elif self.moving_left and self.rect.left > 0 :
            # self.rect.centerx -= 1
            self.center -= self.ai_setting.ship_speed_factor
        # 根据self.center更新rect对象
        self.rect.centerx = self.center

    def blitme(self):
        """在指定位置绘制飞船"""
        self.screen.blit(self.image, self.rect)
