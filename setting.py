class Setting():

    def __init__(self):
        # 屏幕设置
        self.screen_width = 800
        self.screen_heigh = 600
        self.bg_color = (230,230,230)
        # 飞船的设置
        self.ship_speed_factor = 2

        # 子弹的设置
        self.bullet_speed_factor= 1
        self.bullet_width = 2
        self.bullet_height = 10
        self.bullet_color = 60,60,60