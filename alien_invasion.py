import sys

import pygame

from setting import Setting
from ship import Ship
import game_functions as gf
from pygame.sprite import Group


def run_game():
    # 初始化游戏并创建一个屏幕对象
    pygame.init()

    ai_setting = Setting()
    screen = pygame.display.set_mode((ai_setting.screen_width, ai_setting.screen_heigh))
    pygame.display.set_caption("alien Invasion")

    # 创建飞船
    ship = Ship(ai_setting, screen)

    # 创建一个用于存储子弹的编组
    bullets = Group()

    # 设置背景色
    # bg_color  = (230,230,230)

    # 游戏主循环
    while True:
        # 监视键盘和鼠标事件
        # for event in pygame.event.get():
        #     if event.type == pygame.QUIT:
        #         sys.exit()
        gf.check_events(ai_setting, screen, ship, bullets)
        ship.update()
        # # 每次循环重绘屏幕
        # screen.fill(ai_setting.bg_color)
        # ship.blitme()
        # #  让最近绘制的屏幕可见
        # pygame.display.flip()
        bullets.update()

        # 删除已消失的子弹
        for bullet in bullets.copy():
            if bullet.rect.bottom <= 0:
                bullets.remove(bullet)
        gf.update_screen(ai_setting, screen, ship, bullets)


run_game()
